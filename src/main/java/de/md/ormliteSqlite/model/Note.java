package de.md.ormliteSqlite.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "notes")
public class Note {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private boolean appendable;

    @DatabaseField(dataType = DataType.LONG_STRING)
    private String creationDate;

    @DatabaseField(dataType = DataType.LONG_STRING)
    private String customerId;

    @DatabaseField(dataType = DataType.LONG_STRING)
    private String details;

    @DatabaseField(dataType = DataType.LONG_STRING)
    private String reference;

    public Note() {

    }

    public Note(int id, boolean appendable, String creationDate, String scheduleState, String customerId, String details, String references) {
        this.id = id;
        this.appendable = appendable;
        this.creationDate = creationDate;
        this.customerId = customerId;
        this.details = details;
        this.reference = references;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAppendable() {
        return appendable;
    }

    public void setAppendable(boolean appendable) {
        this.appendable = appendable;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
