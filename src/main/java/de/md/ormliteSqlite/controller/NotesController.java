package de.md.ormliteSqlite.controller;

import de.md.ormliteSqlite.model.Note;
import de.md.ormliteSqlite.service.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class NotesController {

    @Autowired
    DatabaseService databaseService;


    @GetMapping("/notes")
    public ResponseEntity<List<Note>> getAllNotes() {
        return new ResponseEntity<List<Note>>(databaseService.getAllNotes(), HttpStatus.OK);
    }

    @GetMapping("/note/{id}")
    public ResponseEntity<Note> getNoteById(@PathVariable Integer id) {
        return new ResponseEntity<Note>(databaseService.getNoteById(id), HttpStatus.OK);
    }

    @PostMapping("/notes")
    public ResponseEntity<Note> createNewNote(@RequestBody Note note) {
        return new ResponseEntity<Note>(databaseService.createNote(note), HttpStatus.OK);
    }

}
