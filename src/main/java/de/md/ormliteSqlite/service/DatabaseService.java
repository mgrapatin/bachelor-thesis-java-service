package de.md.ormliteSqlite.service;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import de.md.ormliteSqlite.model.Note;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class DatabaseService {

    private String databaseURL = "jdbc:sqlite:file::memory:?cache=shared";
    private ConnectionSource connectionSource = new JdbcConnectionSource(databaseURL);
    private Dao<Note, Integer> noteDAO = DaoManager.createDao(connectionSource, Note.class);
    private Logger logger = LoggerFactory.getLogger(DatabaseService.class);
    public DatabaseService() throws SQLException {
        createTableIfNotExists();
    }

    public void createTableIfNotExists() {
        try {
            TableUtils.createTableIfNotExists(this.connectionSource, Note.class);
            logger.info("Der SQL Table wurde erstellt!");
        } catch (SQLException e) {
            logger.error("Die Tabelle konnte nicht angelegt werden: " + e.getMessage());
        }
    }

    public Note createNote(Note note) {
        try {
            noteDAO.create(note);
            return note;
        } catch (SQLException throwables) {
            System.out.println("Die Notiz konnte nicht angelegt werden: " + throwables.getMessage());
        }
        return null;
    }

    public Note getNoteById(int id) {
        try {
            return noteDAO.queryForId(id);
        } catch (SQLException throwables) {
            System.out.println("Es gab einen Fehler beim Abrufen der Notiz: " + throwables.getMessage());
        }
        return null;
    }

    public List<Note> getAllNotes() {
        try {
            return new ArrayList<>(noteDAO.queryForAll());
        } catch (SQLException throwables) {
            System.out.println("Es gab einen Fehler beim Abrufen der Notizen: " + throwables.getMessage());
        }
        return null;
    }
}
